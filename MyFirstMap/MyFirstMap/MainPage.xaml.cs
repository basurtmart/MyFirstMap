﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace MyFirstMap
{
    public partial class MainPage : ContentPage
    {
        private Map map;
        public MainPage()
        {
            InitializeComponent();
            map = new Map
            {
                IsShowingUser = true,
                HeightRequest = 100,
                WidthRequest = 960,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            // Usamos el MapSpan.FromCenterAndRadius
            map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(37, -122), Distance.FromKilometers(0.3)));
            // Agregamos el Slider
            var slider = new Slider(1, 18, 1);
            slider.ValueChanged += (sender, e) =>
            {
                var zoomLevel = e.NewValue; // between 1 and 18
                var latlongdegrees = 360 / (Math.Pow(2, zoomLevel));
                Debug.WriteLine(zoomLevel + " -> " + latlongdegrees);
                if (map.VisibleRegion != null)
                    map.MoveToRegion(new MapSpan(map.VisibleRegion.Center, latlongdegrees, latlongdegrees));
            };
            // Creamos el estilo de los botones map
            var street = new Button
            {
                Text = "Street"
            };
            var hybrid = new Button
            {
                Text = "Hybrid"
            };
            var satellite = new Button
            {
                Text = "Satellite"
            };
            street.Clicked += HandleClicked;
            hybrid.Clicked += HandleClicked;
            satellite.Clicked += HandleClicked;
            var segments = new StackLayout
            {
                Spacing = 30,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    street,
                    hybrid,
                    satellite
                }
            };
            // Ponenos la pagina el together
            var stack = new StackLayout
            {
                Spacing = 0
            };
            stack.Children.Add(map);
            stack.Children.Add(slider);
            stack.Children.Add(segments);
            Content = stack;
            // Para debuggin output only
            map.PropertyChanged += (sender, e) =>
            {
                Debug.WriteLine(e.PropertyName + " just changed!");
                if (e.PropertyName == "VisibleRegion" && map.VisibleRegion != null)
                    CalculateBoundingcoordinates(map.VisibleRegion);
            };
        }

        private void CalculateBoundingcoordinates(MapSpan mapVisibleRegion)
        {
            var center = mapVisibleRegion.Center;
            var halfheightdegrees = mapVisibleRegion.LatitudeDegrees / 2;
            var halfwidthDegrees = mapVisibleRegion.LongitudeDegrees / 2;
            var left = center.Longitude - halfwidthDegrees;
            var right = center.Longitude + halfwidthDegrees;

            var top = center.Latitude + halfheightdegrees;
            var bottom = center.Latitude - halfheightdegrees;

            if (left < -180) left = 180 + (180 + left);
            if (right > 180) right = (right - 180) - 180;

            Debug.WriteLine("Bounding box:");
            Debug.WriteLine(" " + top);
            Debug.WriteLine(" " + left + " " + right);
            Debug.WriteLine(" " + bottom);
        }

        private void HandleClicked(object sender, EventArgs e)
        {
            var b = sender as Button;
            switch (b.Text)
            {
                case "Street":
                    map.MapType = MapType.Street;
                    break;
                case "Hybrid":
                    map.MapType = MapType.Hybrid;
                    break;
                case "Satellite":
                    map.MapType = MapType.Satellite;
                    break;
            }
        }
    }
}
